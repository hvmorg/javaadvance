package DesignPattern.FactoryPattern.Abstract;

public abstract class Audi {

    public abstract String getTypeClass();
    public abstract String getPrice();
    public abstract String getFuel();

    @Override
    public String toString() {
        return "Class : " + this.getTypeClass() + " Price : " + getPrice() + " FUEL : " + getFuel();
    }
}
