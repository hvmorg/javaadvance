package DesignPattern.FactoryPattern.Abstract;

public class FactoryAudi {

    public static Audi getClassAudi(AudiTypeClass typeClass, String price, String fuel) {
        Audi audi = null;
        if (typeClass == null) {

            System.out.println("Failed ...");
        } else {

            switch (typeClass) {
                case A4:
                    audi = new AudiA4(typeClass, price, fuel);
                    break;

                case A8:
                    audi = new AudiA8(typeClass, price, fuel);
                    break;
                default:
                    System.out.println("Failed ...");
                    break;
            }
        }
        return audi;
    }
}
