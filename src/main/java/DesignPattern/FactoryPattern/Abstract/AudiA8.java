package DesignPattern.FactoryPattern.Abstract;

public class AudiA8 extends Audi {
    private AudiTypeClass typeClass;
    private String price ;
    private String fuel;

    public AudiA8(AudiTypeClass typeClass, String price, String fuel) {
        this.typeClass = typeClass;
        this.price = price;
        this.fuel = fuel;
    }

    @Override
    public String getTypeClass() {
        return this.typeClass.toString();
    }

    @Override
    public String getPrice() {
        return this.price;
    }

    @Override
    public String getFuel() {
        return this.fuel;
    }
}
