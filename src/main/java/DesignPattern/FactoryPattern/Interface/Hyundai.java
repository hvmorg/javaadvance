package DesignPattern.FactoryPattern.Interface;

public class Hyundai implements Car {


    @Override
    public void preView() {
        System.out.println("Hyundai view");
    }

    @Override
    public void typeClass() {

        System.out.println("GRAND I10  ---- Price : 763 Tr ");
        System.out.println("ACCENT ---- Price : 559 Tr ");
        System.out.println("ELANTRA  ---- Price : 544 Tr ");
        System.out.println("ELANTRA SPORT  ---- Price : 963 Tr ");
        System.out.println("TUCSON  ---- Price : 963 Tr ");
        System.out.println("SANTAFE  ---- Price : 963 Tr ");

    }
}
