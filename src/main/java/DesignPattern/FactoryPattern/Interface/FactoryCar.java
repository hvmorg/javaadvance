package DesignPattern.FactoryPattern.Interface;

public class FactoryCar {
    public Car showRoom(CarType brand) {
        Car car = null;
        if (brand == null) {

            System.out.println("Please choose your brand ... ");
            return null;

        } else {

            switch (brand) {
                case HONDA:
                    car = new Honda();
                    break;
                case KIA:
                    car = new Kia();
                    break;
                case HYUNDAI:
                    car = new Hyundai();
                    break;
                case MERCEDES:
                    car = new Mercedes();
                    break;
            }
        }
        return car;
    }
}
