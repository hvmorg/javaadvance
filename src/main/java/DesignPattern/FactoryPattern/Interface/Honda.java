package DesignPattern.FactoryPattern.Interface;

public class Honda implements Car {

    @Override
    public void preView() {

        System.out.println("Honda view");
    }

    @Override
    public void typeClass() {
        System.out.println("CIVIC ---- Price : 763 Tr ");
        System.out.println("CITY ---- Price : 559 Tr ");
        System.out.println("JAZZ ---- Price : 544 Tr ");
        System.out.println("CR-V  ---- Price : 963 Tr ");
    }
}
