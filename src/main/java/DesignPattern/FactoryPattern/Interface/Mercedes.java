package DesignPattern.FactoryPattern.Interface;

public class Mercedes implements Car {
    @Override
    public void preView() {
        System.out.println("Mercedes-Benz view ");
    }

    @Override
    public void typeClass() {
        System.out.println("A ---- Price : 763 Tr ");
        System.out.println("C ---- Price : 2559 Tr ");
        System.out.println("CLA ---- Price : 1544 Tr ");
        System.out.println("E  ---- Price : 1963 Tr ");
        System.out.println("S  ---- Price : 4963 Tr ");
    }
}
