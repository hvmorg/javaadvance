package DesignPattern.FactoryPattern.Interface;

public class Client {
    public  static void main (String args[]) {

        FactoryCar factoryCar = new FactoryCar();
        Car carHonda = factoryCar.showRoom(CarType.HONDA);
        carHonda.preView();
        carHonda.typeClass();
        System.out.println("------------------------");
        Car carKia = factoryCar.showRoom(CarType.KIA);
        carKia.preView();
        carKia.typeClass();
        System.out.println("------------------------");
        Car carHyundai = factoryCar.showRoom(CarType.HYUNDAI);
        carHyundai.preView();
        carHyundai.typeClass();
        System.out.println("------------------------");
        Car carMercedes = factoryCar.showRoom(CarType.MERCEDES);
        carMercedes.preView();
        carMercedes.typeClass();
        System.out.println("------------------------");
    }
}
