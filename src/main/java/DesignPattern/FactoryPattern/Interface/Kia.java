package DesignPattern.FactoryPattern.Interface;

public class Kia implements Car {
    @Override
    public void preView() {
        System.out.println("KIA view");
    }

    @Override
    public void typeClass() {
        System.out.println("CERATO ---- Price : 499 Tr ");
        System.out.println("OPTIMA ---- Price : 789 Tr ");
        System.out.println("RONDO ---- Price : 609 Tr ");
    }
}
