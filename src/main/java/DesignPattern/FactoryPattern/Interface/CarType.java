package DesignPattern.FactoryPattern.Interface;

public enum CarType {
    HONDA,
    KIA,
    HYUNDAI,
    MERCEDES

}
