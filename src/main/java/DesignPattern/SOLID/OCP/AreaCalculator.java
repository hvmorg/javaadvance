package DesignPattern.SOLID.OCP;

import java.util.List;

public class AreaCalculator {
    public double Area1(List<Rectangle> shapes) {
        double area = 0;
        for (Rectangle shape : shapes) {
            area += shape.getWidth() * shape.getHeight();
        }
        return area;
    }

    /*
    * IN the case more than 1 shape
    *
    * */
    public double Area2(List<Object> shapes) {
        double area = 0;
        for (Object shape : shapes) {
            if(shape.getClass() == Rectangle.class) {
                Rectangle rectangle = (Rectangle) shape;
                area += rectangle.getWidth() * rectangle.getHeight();
            } else {
                Circle circle = (Circle) shape;
                area += circle.getRadius()*circle.getRadius()*Math.PI;
            }
        }
        return area;
    }

    /*
    * IN the case follow OPC
    *
    * */
    public double Area3(List<Shape> shapes) {
        double area = 0;
        for (Shape shape : shapes) {
            area += shape.Area();
        }
        return area;
    }
}

