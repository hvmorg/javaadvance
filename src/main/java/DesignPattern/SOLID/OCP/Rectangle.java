package DesignPattern.SOLID.OCP;

public class Rectangle implements Shape{
    public double width;
    public double height;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double Area() {
        return width * height;
    }

    public double getArea () {
        return this.height*this.width;
    }
}
