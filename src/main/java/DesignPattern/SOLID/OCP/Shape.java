package DesignPattern.SOLID.OCP;

public interface Shape {
     double Area();
}
