package DesignPattern.SOLID.OCP;

public class Circle implements Shape {
    public double radius ;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double Area() {
        return radius*radius*Math.PI;
    }
}
