package DesignPattern.SOLID.OCP;

import java.util.ArrayList;
import java.util.List;

public class mainOCP {
    public static void main(String argu[]) {
        double sum1 = 0 ;
        double sum2 = 0 ;
        double sum3 = 0 ;
        AreaCalculator areaCalculator = new AreaCalculator();
        Rectangle rectangle1 = new Rectangle();
        rectangle1.setHeight(10);
        rectangle1.setWidth(2);
        List<Rectangle> listRectangle = new ArrayList<>();
        listRectangle.add(rectangle1);
        sum1 = areaCalculator.Area1(listRectangle);
        System.out.println("Sum Area 1 : " + sum1 );

        // more than 1 shape
        Rectangle rectangle2 = new Rectangle();
        rectangle2.setHeight(20);
        rectangle2.setWidth(2);

        Circle circle1 = new Circle();
        circle1.setRadius(10);;

        List<Object> listObject = new ArrayList<>();
        listObject.add(rectangle2);
        listObject.add(circle1);
        sum2 = areaCalculator.Area2(listObject);
        System.out.println("Sum Area 2 : " + sum2 );


        // Using OCP
        List<Shape> listShape = new ArrayList<>();
        Rectangle rectangle3 = new Rectangle();
        rectangle3.setWidth(15);
        rectangle3.setHeight(20);

        Circle circle3 = new Circle();
        circle3.setRadius(10);
        listShape.add(rectangle3);
        listShape.add(circle3);
        sum3 = areaCalculator.Area3(listShape);
        System.out.println("Sum Area 3 : " + sum3 );
    }
}
