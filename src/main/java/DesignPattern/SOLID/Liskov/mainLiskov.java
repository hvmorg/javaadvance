package DesignPattern.SOLID.Liskov;

import DesignPattern.SOLID.OCP.Rectangle;

public class mainLiskov {
    public static void main(String agru[]) {
        Square square = new Square();
        square.setHeight(10);
        System.out.println("Area square : " +square.getArea());

        Rectangle rectangle  = new Rectangle();
        rectangle.setHeight(10);
        rectangle.setWidth(5);
        System.out.println("Area rectangle : " +rectangle.getArea());
    }
}
