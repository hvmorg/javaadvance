package DesignPattern.SOLID.Liskov;

import DesignPattern.SOLID.OCP.Rectangle;

public class Square extends Rectangle {

    public void setWidth(double width) {
        super.setWidth(width);
        super.setHeight(width);
    }

    public void setHeight (double height) {
        super.setHeight(height);
        super.setWidth(height);
    }
}
