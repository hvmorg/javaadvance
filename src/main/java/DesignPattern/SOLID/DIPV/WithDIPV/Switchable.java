package DesignPattern.SOLID.DIPV.WithDIPV;

public interface Switchable {
    void turnOn();
    void turnOff();
}
