package DesignPattern.SOLID.DIPV.WithDIPV;

public interface Switch {

    boolean isOn();

    void press();
}
