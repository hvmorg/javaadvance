package Core;

public class ValueAndReference {
    int data = 10 ;
    void changeValue (int data) {
        data = data + data;
    }
    void changeReference (int data) {
        this.data =  data + data;
    }
    public static void main (String args[]) {

        ValueAndReference valueAndReference = new ValueAndReference();
        System.out.println("---------------------Value----------------------");
        System.out.println("Trước khi thay đổi: " + valueAndReference.data);
        valueAndReference.changeValue(10);
        System.out.println("Sau khi thay đổi: " + valueAndReference.data);
        System.out.println("---------------------Reference----------------------");
        System.out.println("Trước khi thay đổi: " + valueAndReference.data);
        valueAndReference.changeReference(10);
        System.out.println("Sau khi thay đổi: " + valueAndReference.data);

    }
}
