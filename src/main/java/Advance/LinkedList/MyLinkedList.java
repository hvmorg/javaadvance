package Advance.LinkedList;

import java.util.*;

public class MyLinkedList<E> implements Iterable<E> {

    private Pointer<E> tail, first;
    private int size = 0;
    private int numberSelect;

    /**
     * Constructs an empty list.
     */
    public MyLinkedList() {
    }


    public boolean append(E e) {
        final Pointer<E> newPointer = new Pointer<>(tail, e, null);

        if (tail != null)
            tail.setNext(newPointer);
        else {
            first = newPointer;
        }


        this.size = newPointer.getIndex() + 1;
        this.tail = newPointer;
        return true;
    }

    public int size() {
        return size;
    }


    public Iterator<E> iterator() {
        return new RangeIterator();
    }

    public void removeLast() {
        final Pointer<E> pointer = tail;
        if (pointer.getNext() == null) {
            tail.setItem(null);
            tail = tail.getPrev();
            this.size--;
        }
    }

    public void setNumberSelect(int numberSelect) {
        this.numberSelect = numberSelect;
    }

    public RemoveGreater removeItemGreaterThan() {
        return new RemoveGreater();
    }

    private class RangeIterator implements Iterator<E> {
        private Pointer<E> cursor = first;

        @Override
        public boolean hasNext() {
            // loop next point
            if (cursor == null || cursor.getIndex() > tail.getIndex()) {
                return false;
            }
            return true;
        }

        @Override
        public E next() {
            if (this.hasNext()) {
                Pointer<E> current = cursor;
                E item = current.getItem();
                cursor = cursor.getNext();
                return item;
            }
            throw new NoSuchElementException();
        }
    }

    private class RemoveGreater implements Iterator<Integer> {
        private Pointer<E> cursor = first;

        @Override
        public boolean hasNext() {
            Pointer<E> current = cursor;
            Pointer<E> before = cursor.getPrev();
            Pointer<E> after = cursor.getNext();

            if (cursor == null || cursor.getIndex() > tail.getIndex()) {
                return false;
            } else {
                // compare
                int number = Integer.parseInt(current.getItem().toString());
                if (number >  numberSelect) {
                    cursor.setItem(null);
                    // check the case after is the last point
                    if (after.getNext() != null) {
                        cursor = after;
                    } else return false;
                } else {
                    // check the case after is the last point
                    if (after.getNext() != null) {
                        cursor = after;

                    } else return false;
                }
                return true;
            }
        }

        @Override
        public Integer next() {
            if (this.hasNext()) {
                return cursor.getIndex();
            }
            throw new NoSuchElementException();
        }
    }
}
