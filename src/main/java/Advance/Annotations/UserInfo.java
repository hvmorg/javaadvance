package Advance.Annotations;

import java.lang.annotation.Repeatable;

@Repeatable(UsersInfo.class)
public @interface UserInfo {
   String name();
   String fullName();
   int age();
   String nickName();
}
