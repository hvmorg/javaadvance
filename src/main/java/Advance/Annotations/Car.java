package Advance.Annotations;

public class Car extends Vehicule {

    @Override
    public String typeVehicule() {
        String type = "Sedan";
        return type;
    }

    @Override
    public String showVehicule() {
        String show = "Hyundai";
        return show;
    }

}
