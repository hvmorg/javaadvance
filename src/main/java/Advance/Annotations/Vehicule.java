package Advance.Annotations;

@SuppressWarnings("unchecked")
public class Vehicule {

    public String typeVehicule() {
        String type = "hatback";
        return type;
    }

    public String showVehicule() {
        String show = "Audi";
        return show;
    }
    @Deprecated
    public void displayVehicule() {
        System.out.println("Advance.Annotations.Vehicule : " + typeVehicule() + "  " + showVehicule() );
    }

}
