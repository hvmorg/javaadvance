package Advance;/*
 * This Class demonstrates use of annotations using reflection.
 * 
 * @author Yashwant Golecha (ygolecha@gmail.com)
 * @version 1.0
 * 
 */

import Advance.Annotations.*;

import javax.swing.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static Advance.ClassLoader.Loader.classLoader;

public class TodoReport {
    public TodoReport() {
        super();
    }


    public static void main(String[] args) {
        //getTodoReportForBusinessLogic();
        //getType();
        //uncheckedGenerics();
        classLoader();
        //annotationRepeatable();
    }

    /**
     * This method iterates through all messages of Advance.Annotations.BusinessLogic class and fetches annotations defined on each of them.
     * After that it displays the information from annotation accordingly.
     */

    private static void getTodoReportForBusinessLogic() {

        Class businessLogicClass = BusinessLogic.class;
        for (Method method : businessLogicClass.getMethods()) {
            Todo todoAnnotation = (Todo) method.getAnnotation(Todo.class);
            if (todoAnnotation != null) {
                System.out.println(" Method Name : " + method.getName());
                System.out.println(" Author : " + todoAnnotation.author());
                System.out.println(" Priority : " + todoAnnotation.priority());
                System.out.println(" Status : " + todoAnnotation.status());
                System.out.println(" --------------------------- ");
            }
        }
    }


    @SuppressWarnings({"unchecked", "deprecation"})
    private static void getType() {
        Vehicule vehicule = new Vehicule();
        Car car = new Car();

        car.displayVehicule();
        vehicule.displayVehicule();
    }

    @SuppressWarnings("unchecked")
/*
    It's a warning by which the compiler indicates that it cannot ensure type safety.
    It is an annotation to suppress compile warnings about unchecked generic operations (not exceptions), such as casts.
    It essentially implies that the programmer did not wish to be notified about these which he is already aware of when compiling a particular bit of code
*/

    static void uncheckedGenerics() {
        List words = new ArrayList();

        words.add("hello"); // this causes unchecked warning
    }

    @SuppressWarnings("deprecation")
    static void showDialog() {
        JDialog dialog = new JDialog();
        dialog.show();  // this is a deprecated method
    }


    private static void annotationRepeatable() {
        User user = new User();
        UserInfo[] userInfos = user.getClass().getAnnotationsByType(UserInfo.class);
        for(UserInfo userInfo : userInfos) {
            System.out.println("-------------------");
            System.out.println(userInfo.name());
            System.out.println(userInfo.age());
            System.out.println(userInfo.fullName());
            System.out.println(userInfo.nickName());
            System.out.println("-------------------");
        }
    }

}
