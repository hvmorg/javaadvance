package Advance.Concurrerncy.ThreadPool;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Maximum pool size for ThreadPoolExecutor is assigned while instantiating using constructor
 * but we can change the value at runtime too using the method given below.
 * setMaximumPoolSize(): Sets maximum allowed number of threads at runtime.
 * getMaximumPoolSize(): Fetches the maximum allowed number of threads in pool.
 */
public class ThreadPoolExecutorDemoOne {
    public static void main(final String[] args) throws Exception {
        final ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 3, 100, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), new ThreadPoolExecutor.CallerRunsPolicy());
        executor.execute(new BookReader("Ramayan"));
        executor.execute(new BookReader("Mahabharat"));
        executor.execute(new BookReader("Veda"));
        System.out.println("Old Max Pool Size:" + executor.getMaximumPoolSize());
        executor.setMaximumPoolSize(4);
        System.out.println("New Max Pool Size:" + executor.getMaximumPoolSize());
        executor.shutdown();
    }
}

class BookReader implements Runnable {
    private String bookName;

    public BookReader(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println("Reading book: " + bookName);
        }
    }
}