package Advance.Concurrerncy.ThreadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ExamThreadPool {
    public List<ExamPaper> getTestExamPaperList() {
        List<ExamPaper> testExamPaperList = new ArrayList<ExamPaper>();
        for (int i = 0; i < 5; i++) {
            ExamPaper examPaper = new ExamPaper();
            examPaper.setCandicateCode(i);
            examPaper.setCandidateName("name" + i);
            testExamPaperList.add(examPaper);
        }
        return testExamPaperList;
    }

    public static void main(String[] args) {
        int corePoolSize = 50;
        int maxPoolSize = 100;
        long keepAlive = 5000;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(corePoolSize, true);
        RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();

        // creating the ThreadPoolExecutor
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAlive, unit, workQueue, rejectedExecutionHandler);

        // Dummy data for Examiner
        List<ExamPaper> examPaperList ;
        examPaperList = new ExamThreadPool().getTestExamPaperList();

        // Let start all core threads initially
        int coreThread = threadPoolExecutor.prestartAllCoreThreads();

        System.out.println("Core Thread " + coreThread);

        // submit work to the thread pool
        for (int i = 0; i < 10; i++) {
            threadPoolExecutor.execute(new Examiner(examPaperList));
        }

        // shut down the pool
        threadPoolExecutor.shutdown();
    }
}

class Examiner implements Runnable {

    List<ExamPaper> examPaperList = new ArrayList<ExamPaper>();

    public Examiner() {

    }

    public Examiner(List<ExamPaper> examPaperList) {
        for (ExamPaper examPaper : examPaperList) {
            this.examPaperList.add(examPaper);
        }
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Start ");
        doExaminer();
        System.out.println(Thread.currentThread().getName() + " End.");
    }

    private void doExaminer() {
        if (examPaperList.size() > 0) {
            System.out.println("I am " + Thread.currentThread().getName() + "I am grading your exam papers now.");
            for (ExamPaper examPaper : examPaperList) {
                System.out.println("Candidate name is: " + examPaper.getCandidateName());
            }
        }
    }
}

class ExamPaper {
    String candidateName;
    Integer candicateCode;

    /**
     * @return the candidateName
     */
    public String getCandidateName() {
        return candidateName;
    }

    /**
     * @param candidateName the candidateName to set
     */
    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    /**
     * @return the candicateCode
     */
    public Integer getCandicateCode() {
        return candicateCode;
    }

    /**
     * @param candicateCode the candicateCode to set
     */
    public void setCandicateCode(Integer candicateCode) {
        this.candicateCode = candicateCode;
    }
}