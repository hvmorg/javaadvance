package Advance.Concurrerncy.SynchronizedAndConcurentCollection;

import java.util.*;

public class SynchronizedListFromArrayList {
    public static void main(String[] args) {

        // ********************** synchronizedList ************************
        ArrayList<String> arrayList = new ArrayList<>();

        // populate the crunchifyArrayList
        arrayList.add("eBay");
        arrayList.add("Paypal");
        arrayList.add("Google");
        arrayList.add("Yahoo");

        // Returns a synchronized (thread-safe) list backed by the specified
        // list. In order to guarantee serial access, it is critical that all
        // access to the backing list is accomplished through the returned list.
        List<String> synchronizedList = Collections.synchronizedList(arrayList);

        System.out.println("synchronizedList conatins : " + synchronizedList);

        // ********************** synchronizedMap ************************
        Map<String, String> map = new HashMap<>();

        // create a synchronized map
        Map<String, String> synchronizedMap = Collections.synchronizedMap(map);

        // populate the crunchifyMap
        map.put("1", "eBay");
        map.put("2", "Paypal");
        map.put("3", "Google");
        map.put("4", "Yahoo");



        System.out.println("synchronizedMap contains : " + synchronizedMap);
    }
}
