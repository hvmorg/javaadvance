package Advance.Concurrerncy.SynchronizedAndConcurentCollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SynchronizedListExample {
    public static void main(String[] args) {
        SynchronizedList synchronizedList = new SynchronizedList();
        Thread t1 = new Thread(synchronizedList);
        Thread t2 = new Thread(synchronizedList);
        t1.start();
       // System.out.println(" ON calss main Thread current  " + t1.getName() + " Status : " + t1.getState());
        t2.start();
       // System.out.println(" ON calss main Thread current  " + t2.getName() + " Status : " + t2.getState());
    }
}
class SynchronizedList implements Runnable {

    @Override
    public void run() {

        System.out.println("ON calss SynchronizedList Thread current  " + Thread.currentThread().getName());
        createList();
    }

    public synchronized List<String> createList (){
        String str ;
        List<String> syncList = Collections.synchronizedList(new ArrayList<String>());
        for (int i = 0 ; i < 10 ; i ++) {
            str = String.valueOf(i);
            syncList.add(str);
    }

        // when iterating over a synchronized list, we need to synchronize access to the synchronized list
        synchronized (syncList) {
            System.out.println("Thread current  " + Thread.currentThread().getName() + " Status : " + Thread.currentThread().getState());

            Iterator<String> iterator = syncList.iterator();
            while (iterator.hasNext()) {
                System.out.println("item: " + iterator.next());
            }
        }
        return syncList;
    }

}
