package Advance.Concurrerncy.BlockQueueAndProduceCusumer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class MultiThreadQueue {
    public static void main(String[] args) {
        BlockingQueue drop = new LinkedBlockingQueue(1024); // 3
        Producer producer = new Producer(drop);
        Consumer consumer = new Consumer(drop);
        new Thread(producer).start();
        new Thread(consumer).start();
    }
}

class Producer implements Runnable {
    private BlockingQueue drop;

    List messages = Arrays.asList(
            "Dinh Tien Hoang",
            "Quang Trung",
            "Ly Thai To",
            "Thang Long",
            "Gia Dinh");

    public Producer(BlockingQueue d) {
        this.drop = d;
    }

    public void run() {
        try {
            for (Object s : messages) {
               // Thread.sleep(3000);
                boolean test = drop.offer(s, 3, TimeUnit.SECONDS); // 1
                if (test == false) {
                    System.out.println("BlockingQueue is full now. Can't set value from BlockingQueue. Release Producer");
                }
            }
            drop.put("DONE");
        } catch (InterruptedException intEx) {
            System.out.println("Interrupted! " + "Last one out, turn out the lights!");
        }
    }
}

class Consumer implements Runnable {
    private BlockingQueue drop;

    public Consumer(BlockingQueue d) {
        this.drop = d;
    }

    public void run() {
        try {
            String msg = null;
            while (true) {
                msg = (String) drop.poll(3, TimeUnit.SECONDS); // 2
                if (msg == null) {
                    System.out.println("BlockingQueue is empty now. Can't get value from BlockingQueue. Release Consumer");
                }
                if (msg.equals("DONE")) {
                    break;
                }

                System.out.println(msg);
            }
        } catch (InterruptedException intEx) {

            System.out.println("Interrupted! " + "Last one out, turn out the lights!");
        }
    }
}