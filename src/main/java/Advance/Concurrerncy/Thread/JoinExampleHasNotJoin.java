package Advance.Concurrerncy.Thread;

import java.util.logging.Logger;

public class JoinExampleHasNotJoin {
    public static void main(String[]args){
        Thread th1 = new Thread(new MyThread(),"th1");
        Thread th2 = new Thread(new MyThread(),"th2");
        Thread th3 = new Thread(new MyThread(),"th3");
        th1.start();
        th2.start();
        th3.start();
    }
}
class MyThread implements Runnable {
    private static final Logger LOGGER = Logger.getLogger( MyThread.class.getName() );
    public void run() {
        Thread t = Thread.currentThread();
        LOGGER.info("Begin thread: " + t.getName());
        try{
            Thread.sleep(4000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        LOGGER.info("End thread:" + t.getName());
    }
}
