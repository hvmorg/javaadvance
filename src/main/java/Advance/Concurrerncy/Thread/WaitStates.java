package Advance.Concurrerncy.Thread;

import java.util.logging.Logger;

public class WaitStates {
    private static final Logger LOGGER = Logger.getLogger( WaitStates.class.getName() );
    public static void main(String[] args){
        ThreadB b = new ThreadB();
        b.start();

       synchronized(b){
            try{
                LOGGER.info("Waiting for b to complete...");
                b.wait();
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            LOGGER.info("Total is: " + b.total);
        }
        // it doesnt wait
        //LOGGER.info("Total is: " + b.total);
    }
}

class ThreadB extends Thread{
    int total;
    @Override
    public void run(){
        synchronized(this){
            for(int i=0; i<100 ; i++){
                total += i;
            }
            notify();
        }
    }
}