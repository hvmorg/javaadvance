package Advance.Concurrerncy.Thread;

import java.util.logging.Logger;

public class WaitingState implements Runnable {
    public static Thread t1;
    private static final Logger LOGGER = Logger.getLogger( WaitingState.class.getName() );
    public static void main(String[] args) {
        t1 = new Thread(new WaitingState());
        t1.start();
        LOGGER.info("Thread 1  : " + WaitingState.t1.getState());
    }

    public void run() {
        Thread t2 = new Thread(new DemoThreadWS());
        t2.start();

        try {
            t2.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.warning("Thread interrupted" + e);
        }
       //LOGGER.info("Thread 1  : " + WaitingState.t1.getState());
    }
}

class DemoThreadWS implements Runnable {
    private static final Logger LOGGER = Logger.getLogger( DemoThreadWS.class.getName() );
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.warning("Thread interrupted" + e);
        }

        LOGGER.info("Thread 1 : " + WaitingState.t1.getState());
    }
}
