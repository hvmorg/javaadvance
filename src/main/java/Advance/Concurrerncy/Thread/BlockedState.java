package Advance.Concurrerncy.Thread;


import java.util.logging.Logger;

public class BlockedState {
    private static final Logger LOGGER = Logger.getLogger( BlockedState.class.getName() );
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new DemoThreadB());
        Thread t2 = new Thread(new DemoThreadB());

        t1.start();
        t2.start();

        Thread.sleep(1000);

        //log.info(t2.getState());
        LOGGER.info("Thread 1 : " + t1.getState());

        //t2.start();

        LOGGER.info("Thread 2 : " + t2.getState());
        System.exit(0);
    }
}

class DemoThreadB implements Runnable {
    @Override
    public void run() {
            commonResource();
    }

    public static synchronized void commonResource(){
        while(true) {
            // Infinite loop to mimic heavy processing
            // 't1' won't leave this method
            // when 't2' try to enters this
        }
        //Thread.sleep(10000);
    }
}
