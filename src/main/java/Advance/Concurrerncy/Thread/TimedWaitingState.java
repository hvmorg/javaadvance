package Advance.Concurrerncy.Thread;

import java.util.logging.Logger;

public class TimedWaitingState {
    private static final Logger LOGGER = Logger.getLogger( TimedWaitingState.class.getName() );
    public static void main(String[] args) throws InterruptedException {
        DemoThread obj1 = new DemoThread();
        Thread t1 = new Thread(obj1);
        t1.start();

        // The following sleep will give enough time for ThreadScheduler
        // to start processing of thread t1
        Thread.sleep(1000);
        //wait();
        t1.wait(10000, 500);
        LOGGER.info ("States Thread 1 : " + t1.getState());
    }
}

class DemoThread implements Runnable {
    private static final Logger LOGGER = Logger.getLogger( DemoThread.class.getName() );
    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.warning ("Thread interrupted" + e);
        }
    }
}