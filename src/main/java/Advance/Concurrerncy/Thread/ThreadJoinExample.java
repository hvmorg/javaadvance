package Advance.Concurrerncy.Thread;

import java.util.logging.Logger;

public class ThreadJoinExample {
    private static final Logger LOGGER = Logger.getLogger( ThreadJoinExample.class.getName() );
    public static void main(String[] args) {
        Thread t1 = new Thread(new MyRunnable(), "t1");
        Thread t2 = new Thread(new MyRunnable(), "t2");
        Thread t3 = new Thread(new MyRunnable(), "t3");

        t1.start();

        //start second thread after waiting for 2 seconds or if it's dead
        try {
            t1.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t2.start();

        //start third thread only when first thread is dead
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t3.start();

        //let all threads finish execution before finishing main thread
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        LOGGER.info("All threads are dead, exiting main thread");
    }

}

class MyRunnable implements Runnable{
    private static final Logger LOGGER = Logger.getLogger( MyRunnable.class.getName() );
    @Override
    public void run() {
        LOGGER.info("Thread started:::"+Thread.currentThread().getName());
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info("Thread ended:::"+Thread.currentThread().getName());
    }

}
