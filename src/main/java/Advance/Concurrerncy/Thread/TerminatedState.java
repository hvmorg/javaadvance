package Advance.Concurrerncy.Thread;

import java.util.logging.Logger;

public class TerminatedState implements Runnable {
    private static final Logger LOGGER = Logger.getLogger( TimedWaitingState.class.getName() );
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new TerminatedState());
        t1.start();
        LOGGER.info("Threat status : " +t1.getState());
        // The following sleep method will give enough time for
        // thread t1 to complete
        Thread.sleep(1000);
        LOGGER.info("Threat status : " +t1.getState());
    }

    @Override
    public void run() {
        // No processing in this block
    }
}