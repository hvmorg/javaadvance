package Advance.Concurrerncy.CallableFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Calcul {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // Create thread pool using Executor Framework
        ExecutorService executor = Executors.newFixedThreadPool(2);

        List<Future<Integer>> list = new ArrayList<Future<Integer>>();
        for (int i = 0; i < 100; i++) {
            // Create new Calculator object

            Calculator c = new Calculator(i, i + 1);
            Future f1 =  executor.submit(c);
            System.out.println(f1.isDone());
             if(i == 50 ) {
                 f1.cancel(true);
             }
            list.add(f1);

        }

        for (Future f : list) {
            if(!f.isCancelled())
            System.out.println("F : " +f.get());

            /* if(f.get().equals(5)) {
                System.out.println("Cancel");
            } else if (f.get().equals(8)){
                System.out.println("Cancel");
            } else {
                System.out.println(f.get());
            }*/
        }

        executor.shutdown();
    }
}
class Calculator implements Callable<Integer> {

    private int a;
    private int b;

    public Calculator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public Integer call() throws Exception {
        Thread.sleep(200);
        return a;
    }
}