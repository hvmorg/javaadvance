package Advance.Concurrerncy.ThreadPrioriry;


public class ThreadUseYield implements Runnable  {
    Thread t;

    ThreadUseYield(String str) {

        t = new Thread(this, str);
        // this will call run() function
        t.start();
    }

    public void run() {

        for (int i = 0; i < 10000; i++) {
            // yields control to another thread every 5 iterations
            if ((i % 10000) == 0) {
                System.out.println(Thread.currentThread().getName() + " yielding control...");

            /* causes the currently executing thread object to temporarily
            pause and allow other threads to execute */
               Thread.yield();
            }
        }

        System.out.println(Thread.currentThread().getName() + " has finished executing.");
    }

    public static void main(String[] args) {
        new ThreadUseYield("Thread 1");
        new ThreadUseYield("Thread 2");
        new ThreadUseYield("Thread 3");
    }
}
