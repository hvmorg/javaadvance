package Advance.Concurrerncy.ThreadSafety;

public class ThreadSafetyDemo {

    public static void main(String[] args) {
        BusReservation br = new BusReservation();
        PassengerThread pt1 = new PassengerThread(2, br, "SAM");
        PassengerThread pt2 = new PassengerThread(2, br, "Jack");
        pt1.start();
        pt2.start();
    }
}

class BusReservation implements Runnable {
    private int totalSeatsAvailable = 2;

    public void run() {
        //BusReservation br = new BusReservation();
        PassengerThread pt = (PassengerThread) Thread.currentThread();
        boolean ticketsBooked = false;
        try {
            ticketsBooked = this.bookTickets(pt.getSeatsNeeded(), pt.getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (ticketsBooked == true) {
            System.out.println("CONGRATS" + Thread.currentThread().getName() +
                    " ..The number of seats requested(" + pt.getSeatsNeeded()
                    + ")are BOOKED");
        } else {
            System.out.println("Sorry Mr." + Thread.currentThread().getName()
                    + " .. The number of seats requested(" + pt.getSeatsNeeded()
                    + ") are not available");
        }
    }

    //public boolean bookTickets(int seats, String name) throws InterruptedException {
    public synchronized boolean bookTickets(int seats, String name) throws InterruptedException{
        System.out.println("Welcome to HappyBus " + Thread.currentThread().getName()
                + " Number of Tickets Available are:"
                + this.getTotalSeatsAvailable());

        if (seats > this.getTotalSeatsAvailable()) {

            return false;

        } else {
            Thread.sleep(300);
            totalSeatsAvailable = totalSeatsAvailable - seats;
            return true;
        }
    }

    private int getTotalSeatsAvailable() {
        return totalSeatsAvailable;
    }

}

//This represent a Passenger. The reasons for extending the Thread class is that this should carry the data of number of seats.
class PassengerThread extends Thread {

    private int seatsNeeded;

    public PassengerThread(int seats, Runnable target, String name) {
        super(target, name);
        this.seatsNeeded = seats;
    }

    public int getSeatsNeeded() {
        return seatsNeeded;
    }

}

