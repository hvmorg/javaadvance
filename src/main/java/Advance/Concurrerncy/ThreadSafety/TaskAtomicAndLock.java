package Advance.Concurrerncy.ThreadSafety;

import javafx.concurrent.Task;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TaskAtomicAndLock {

    public static void main(String[] args) throws InterruptedException {

        TaskAtomic taskAtomic = new TaskAtomic() ;

        TaskLock taskLock = new TaskLock();

        int numberThread = 50;
        Thread thread[] = new Thread[numberThread];

        Date begin , end ;
        // for task Lock
        begin = new Date();

        for(int i = 0 ; i< numberThread ; i++) {
            thread[i] = new Thread(taskLock);
            thread[i].start();
            try{
                thread[i].join();
            } catch ( InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        end = new Date();
        System.out.println("Main lock result :  " + (end.getTime() - begin.getTime()));

        // for task Atomic
        begin = new Date();

        for(int i = 0 ; i< numberThread ; i++) {
            thread[i] = new Thread(taskAtomic);
            thread[i].start();
            thread[i].join();
        }

        end = new Date();
        System.out.println("Main Atomic result :  " + (end.getTime() - begin.getTime()));
    }

}

class TaskAtomic implements Runnable {
    private final AtomicInteger number ;

    public TaskAtomic (){
        this.number = new AtomicInteger();
    }

    @Override
    public  void run() {
        for (int i = 0 ; i < 1000000; i++) {
            number.set(i);
        }
    }
}

class TaskLock implements Runnable {

    private int number;
    private Lock lock ;
    public  TaskLock() {
        this.lock = new ReentrantLock();
    }
    @Override
    public void run() {
        for (int i = 0 ; i < 1000000 ; i++) {
            lock.lock();
            number = i ;
            lock.unlock();

        }
    }


}