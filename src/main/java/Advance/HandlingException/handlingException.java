package Advance.HandlingException;

import java.util.Scanner;

public class handlingException {

    public static void main(String[] args) {

        System.out.println("!!!!BEGIN!!!");

        try {
            inputText();
            System.out.println("Input Success");
        } catch (NotFoundException e) {
            System.out.println(e);
        } finally {
            System.out.println("!!!!END!!!");
        }
    }


    private static void inputText() throws NotFoundException {
        System.out.println("Input text : ");
        try {
            Scanner input = new Scanner(System.in);
            String inputStr = input.nextLine();
            isEmpty(inputStr);
        } catch (MyException ex) {
            throw new NotFoundException(ex.getMessage());
        }

    }

    private static boolean isEmpty(String str) throws MyException {
        if (str.equals("")) {
            throw new MyException("String input is empty");
        }
        return true;
    }

}
