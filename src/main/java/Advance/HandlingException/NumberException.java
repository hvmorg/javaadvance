package Advance.HandlingException;

public class NumberException extends Exception {

    private String message;

    public NumberException() {
    }

    public NumberException(String message) {
        super(message);
        this.message = message;
    }

    public NumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public NumberException(Throwable cause, String message) {
        super(cause);
        this.message = message;
    }

    public NumberException(NumberException ex) {

    }

    @Override
    public String toString() {
        return "MyException{" +
                "message='" + message + '\'' +
                '}';
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
