package Advance.HandlingException;

public class MyException extends  Exception {

    private String message;

    public MyException() {
    }

    public MyException(String message) {
        super(message);
        this.message = message;
    }

    public MyException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyException(Throwable cause, String message) {
        super(cause);
        this.message = message;
    }

    @Override
    public String toString() {
        return "MyException{" +
                "message='" + message + '\'' +
                '}';
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
