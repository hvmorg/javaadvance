package Advance.Generic;

import java.util.ArrayList;
import java.util.List;

public class mainGenerics {
    public static void main(String[] args) {
        List listAlphabet = new ArrayList<String>();
        String str = "abc";
        int number = 4;
        double dub = 4.66;
        boolean flg = true;

        listAlphabet.add("b");
        listAlphabet.add("bb");
        listAlphabet.add("bbb");
        listAlphabet.add("bbbb");
        listAlphabet.add("bbbbb");
        listAlphabet.add("bbbbbb");
        String firstAlphabet = Utils.getMax(listAlphabet);
        System.out.println("MAX : " + firstAlphabet);

        MyArrrayList<String> myArrrayList = new MyArrrayList<>();
        myArrrayList.add(str);
        myArrrayList.add("hello");
        myArrrayList.displayList();
        MyArrrayList<Integer> myArrrayList1 = new MyArrrayList<>();
        myArrrayList1.add(number);
        myArrrayList1.add(6);
        myArrrayList1.displayList();
        MyArrrayList<Double> myArrrayList2 = new MyArrrayList<>();
        myArrrayList2.add(dub);
        myArrrayList2.add(6.636363);
        myArrrayList2.displayList();
        MyArrrayList<Boolean> myArrrayList3 = new MyArrrayList<>();
        myArrrayList3.add(flg);
        myArrrayList3.add(false);
        myArrrayList3.displayList();

        MyGenericArrayList<Integer> myGenericArrayList = new MyGenericArrayList<>();
        for(int i = 0 ; i < 10 ; i++) {
            myGenericArrayList.add(i+1);
        }
        myGenericArrayList.display();

        PersonModel<Student> student = new PersonModel<>();

        Student stu1 = new Student();
        Student stu2 = new Student();

        stu1.setId("1");
        stu1.setName("Vu Hoang");
        stu1.setAge(30);

        stu2.setId("2");
        stu2.setName("Minh Quang");
        stu2.setAge(20);

        student.add(stu1);
        student.add(stu2);

        student.display();

    }
}
