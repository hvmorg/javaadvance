package Advance.Generic;

import java.util.ArrayList;
import java.util.List;

public class MyArrrayList<T> {

    private List<T> list = new ArrayList<>();

    public void add(T t) {
            list.add(t);
    }

    public <T> void displayList() {
        if (list != null) {
            System.out.println("*--------------------------------------------*");
            for (int i = 0; i < list.size(); i++) {
                System.out.println("Item " + list.get(i).getClass() + " " + (i + 1) + " : " + list.get(i));
            }
        }
    }
}
