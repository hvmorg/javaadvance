package Advance.Generic;

import java.util.List;

public class Utils {
    public static <T> boolean isEmptyCollection(T input) {
        if (input.equals("") || input == null) {
            return false;
        }
        return true;
    }

    public static <T> boolean isNotEmptyColletion(T input) {
        if (!isEmptyCollection(input)) {
            return false;
        }
        return true;
    }

    public static <E> void sortOrderArray(E[] input) {
        if (isEmptyCollection(input)) {

        }
        for (E element : input) {

        }
    }

    /**
     * get max object
     * @value  : Object
     * @return : max
     * @param : input
     * */
    public static String getMax(List<? extends String> input) {
        String max = null;
        if (isNotEmptyColletion(input)) {
            max = input.get(0);
            for (String o : input) {
                if (max.compareTo(o) > 0) {
                    max = o ;
                }
            }
        }
        return max;
    }
}
