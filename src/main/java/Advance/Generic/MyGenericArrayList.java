package Advance.Generic;

import java.util.ArrayList;
import java.util.List;

public class MyGenericArrayList<T extends Integer> {
    private List<T> list = new ArrayList<>();

    public void add(T t) {
        list.add(t);
    }

    public <T> void display() {

        if (list.size() > 0 && list != null) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println("Number : " + list.get(i));
            }
        }
    }
}
