package Advance.MemoryManagement;

import Advance.MemoryManagement.memoryLeak.AutoBoxing;
import Advance.MemoryManagement.memoryLeak.Cache;
import Advance.MemoryManagement.memoryLeak.CustomKey;

public class mainMemoryManagement {
    public static void main(String[] args) {

        AutoBoxing autoBoxing = new AutoBoxing();
        long sum = 0;
        for (int i = 0; i < 1000; i++) {
            sum += autoBoxing.addIncremental(i);
        }
        System.out.println("Sum : " + sum);

        Cache cache = new Cache();
        cache.initCache();
        cache.forEachDisplay();
        // if we don't call method clear cache --> object of Hashmap still existed on heap and the GC can't know they are unused
        //---> memory leak
        //---> Call method clear or use WeakHasMap instead of HasHMap
        cache.clear();
        CustomKey.execute();
    }
}
