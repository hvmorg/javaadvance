package Advance.MemoryManagement.memoryLeak;

import java.util.HashMap;
import java.util.Map;

/**
 * Trong class CustomKey này, chúng ta quên không implement 2 method là equals() và hashCode().
 * Nói thêm một chút, để truy cập tới một object của map, method get() sẽ check hashCode() và equals() cho từng object.
 * Như vậy, hậu qủa của việc không implement 2 method kia là key và value lưu trong map không thể lấy ra được.
 * Việc trong map tồn tại các cặp <key,value> nhưng application không thể "dùng" được chúng chắc chắn được liệt vào danh sách memory leaks.
 * Như vậy, khi tạo CustomKey, đừng quên equals() và hashCode().
 */

public class CustomKey {
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomKey customKey = (CustomKey) o;

        return name != null ? name.equals(customKey.name) : customKey.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public CustomKey(String name) {
        this.name = name;

    }

    public static void execute() {

        Map<CustomKey, String> map = new HashMap<CustomKey, String>();

        map.put(new CustomKey("Shamik"), "Shamik Mitra");

        String val = map.get(new CustomKey("Shamik"));

        System.out.println("Missing equals and hascode so value is not accessible from Map " + val);

    }
}
