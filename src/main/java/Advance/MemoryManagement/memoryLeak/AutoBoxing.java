package Advance.MemoryManagement.memoryLeak;

public class AutoBoxing {
    public long addIncremental(long l) {
        // use Wapper class --> autoboxing will be create new object.
        Long sum = 0L;
        sum = sum + 1;
        return sum;
    }

}
