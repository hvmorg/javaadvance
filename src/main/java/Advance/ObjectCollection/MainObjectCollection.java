package Advance.ObjectCollection;

public class MainObjectCollection {
    private String test ;

    public static void main(String[] args) {

        School school = new School();
        school.setAdd("1 Xa Dan");
        school.setName("Kim Lien");
        school.setTelephoneNumber("0912345678");

        School school1 = new School();
        school1.setAdd("2 Xa Dan");
        school1.setName("Kim Lien");
        school1.setTelephoneNumber("0912345678");


        boolean flg = school.equals(school1);
        System.out.println("School : "+school.toString());
        System.out.println("School1 : "+school1.toString());
        System.out.println(flg);

    }


}
