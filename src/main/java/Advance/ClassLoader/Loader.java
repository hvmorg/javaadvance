package Advance.ClassLoader;

public class Loader {

    public static void classLoader() {
        System.out.println("class loader for HashMap: "
                + java.util.HashMap.class.getClassLoader());
        System.out.println("class loader for DNSNameService: "
                + sun.net.spi.nameservice.dns.DNSNameService.class
                .getClassLoader());
        System.out.println("class loader for this class: "
                + Loader.class.getClassLoader());

       // System.out.println(com.mysql.jdbc.Blob.class.getClassLoader());
    }
}
